# RPG Projects

This will be all the projects from the Udemy courses for the RPG in Unity.
There is a total of four courses starting with core combat, then quests and dialogue,
inventory systems, and finally shops and abilities.

## Commits

* Initial commit - Repo created
* Project created with terrain and follow camera
* Imported asset packs for creating character and animation
* Completed basic movement and beginning to create the folder structure
* Begin implementing basic combat
* Fixed issue with movement
* Removed the circle dependency on combat to cancel the current action using an interface
* Added Health component to begin the process of taking damage
* Created a base prefab and two variants for Player and Enemy
* Added death animation to the animator controller
* Completed basic combat and fixed some issues ("bugs")
* Changed IDE to Jetbrains Rider
* Begin adding AI behaviours and Patrol Routes
  * Removed Zip-Files from the root project folder
* Completed the initial AI configurations
* Begin creating a "moment" in the game design
